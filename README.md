# cgit alpine container

This is a Git repo for building a `cgit` container using an 
[Alpine Linux](https://hub.docker.com/_/alpine) base image.

By default, it will serve any Git repos found in the `/srv/git/` directory.
It is configured for syntax highlighting and about page formatting.

## How to use this image

```
$ docker build -t cgit:alpine .
$ docker run -p 80:80 -v /git/repo/directory:/srv/git:ro -d cgit:alpine
```

### Using a custom configuration

To provide a custom `cgit` configuration:

```shell
$ docker run -p 80:80 -d \
-v /path/to/custom/config:/etc/cgitrc \
-v /git/repo/directory:/srv/git:ro \
cgit:alpine
```

For more information on a custom configuration, please see the 
[cgit documentation](https://git.zx2c4.com/cgit/tree/cgitrc.5.txt)
