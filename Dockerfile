FROM alpine:3.18.4

RUN apk add --no-cache nginx \
            cgit \
            git \
            fcgiwrap \
            spawn-fcgi \
            python3 \
            py3-pip \
            groff \
            tini

RUN set -eux; \
    ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && mkdir -p /run/nginx /srv/git \
    && pip3 install Markdown docutils pygments

COPY nginx-server.conf /etc/nginx/http.d/default.conf
COPY cgitrc.example /etc/cgitrc
COPY docker-entrypoint.sh /usr/local/bin/

EXPOSE 80

VOLUME [ "/srv/git" ]
VOLUME [ "/var/cache/cgit" ]

ENTRYPOINT ["/sbin/tini", "--"]
CMD [ "docker-entrypoint.sh" ]
