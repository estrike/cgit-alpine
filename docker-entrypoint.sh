#!/bin/sh

/usr/bin/spawn-fcgi -M 666 -u nginx -g www-data -s /var/run/fcgi.socket /usr/bin/fcgiwrap

exec /usr/sbin/nginx -g "daemon off;"
